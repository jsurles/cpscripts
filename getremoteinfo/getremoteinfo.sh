## SYSTEM
TSTAMP=`/bin/date +%Y%m%d%H%M%S`
FILEOUT="/tmp/remoteinfo_$TSTAMP.txt"
clear

NOTIFY () {
	/bin/echo "################################################################################################################"
	/bin/echo "Data has been collected about your server into the file $FILEOUT"
	/bin/echo
	/bin/echo "Please review this information, and if acceptable submit it to cPanel"
	/bin/echo "by either attaching the file or pasting the comments to the ticket."
	/bin/echo
	/bin/echo "Be sure to delete this file ( $FILEOUT ) if you don't want to keep it on your server."
	/bin/echo "################################################################################################################"A
}

NL () {
echo >> $FILEOUT
}

TITLE () {
echo "===> $1 <===" >> $FILEOUT
echo "RUNNING: $1 "
}

RUN () {
TITLE "$1"
sh -c "$1" >> $FILEOUT 2>&1
NL
}

STATIT () {
TITLE "/usr/bin/stat $1"
/usr/bin/stat "$1" >> $FILEOUT 2>&1
NL
}

DUMPIT () {
TITLE "/bin/cat $1"
/bin/cat "$1"  >> $FILEOUT 2>&1
NL
}

FILEDATA () {
if [ -e "$1" ];then
  STATIT "$1"
  DUMPIT "$1"
 else
  echo "$1 does not exist." >> $FILEOUT
  NL
fi
}

## Gather System Information

RUN "/bin/date"
FILEDATA "/etc/redhat-release"
RUN "/usr/bin/uptime"
RUN "/bin/hostname"
RUN "/bin/uname -a"
RUN '/bin/grep ^processor /proc/cpuinfo'
RUN "/usr/bin/free -h"
RUN "/bin/df -h"
RUN "/bin/df -i"
RUN "/usr/bin/curl -s -4 http://ifconfig.co"
RUN "/sbin/ifconfig -a"
RUN "/sbin/ip addr"
RUN "/bin/netstat -rn"
RUN "/sbin/ip route"
RUN "/bin/netstat -plan"
FILEDATA "/etc/resolv.conf"
FILEDATA "/etc/hosts"
RUN "/usr/bin/sar"
RUN "/sbin/iptables -L -n -t nat"
RUN "/sbin/iptables -L -n"
RUN "/bin/ls /var/named/*.db | wc -l"
RUN "/bin/ls -l /etc/named.conf"
FILEDATA "/etc/named.conf"
RUN "/usr/bin/top -bn 1"
RUN "/bin/ps auxwwwf"
RUN "/usr/sbin/lsof"

## Gather cPanel Specific data

RUN "/usr/bin/curl --insecure -s https://ssp.cpanel.net/run | /bin/sh | /bin/sed -r \"s:\x1B\[[0-9;]*[mK]::g\""
FILEDATA "/var/cpanel/updatelogs/summary.log"
FILEDATA "/var/cpanel/icontact_event_importance.json"
FILEDATA "/var/cpanel/cpanel.config"
FILEDATA "/etc/ips"
FILEDATA "/var/cpanel/cpnat"
FILEDATA "/usr/localcpanel/version"
FILEDATA "/var/cpanel.conf"
RUN "/usr/bin/curl -s -4 http://cpanel.com/showip.shtml"
RUN "/usr/bin/curl -s https://bitbucket.org/jsurles/cpscripts/raw/master/clicense/clicense.sh | /bin/sh"

## Gather Connectivity Information

RUN "/bin/ping -c5 www.google.com"
RUN "/usr/bin/dig +trace www.google.com @127.0.0.1"
RUN "/bin/traceroute -T -p 53 -n a.gtld-servers.net"

NOTIFY
