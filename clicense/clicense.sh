#!/bin/sh

IP=$1

if [ "$IP" == "" ];then
	CPIP=`/usr/bin/curl -s -4 http://cpanel.com/showip.shtml`
	PBIP=`curl -s4 http://ifconfig.co`
	if [ "$CPIP" == "$PBIP" ];then
		IP=$CPIP
	else
		echo "I looks you like you may have different IPs or routes for cPanel usage."
		echo "cPanel Detects: $CPIP"
		echo "Public Detects: $PBIP"
		echo
		echo "Please specify by adding the intended IP address to the end of the command."
		echo "Your choices might be:"
		/sbin/ip address | grep -w inet | awk '{print $2}' | awk -F\/ '{print $1}' | grep -v '127.0.0.1'
		exit
	fi
fi
curl -s4 https://verify.cpanel.net/index.cgi?ip=$IP 2>/dev/null | grep -A1000 '<table class="history table">' | grep -B1000 '<h3>Standard Package Types</h3>' | grep -B1000 '</table>' > /tmp/cpdata.curltmp

OUTPUT () {
	echo "                 #: $ENTRY"
	echo "                IP: $IP"
	echo "           Package: $PKG"
	echo "       Partner NOC: $PNOC"
	echo "          Activity: $ACTIVE"
	echo "           Product: $PROD"
	echo "            Status: $STATUS"
	echo "Updates Expiration: $UPEX"
	echo 
}

echo "https://verify.cpanel.net/index.cgi?ip=$IP"
echo "<c>"

for FLINE in `grep -n '<tr class="status' /tmp/cpdata.curltmp| awk -F: '{print $1}'`
do
	let LLINE=FLINE+17
	sed -n "$FLINE,$LLINE p" /tmp/cpdata.curltmp > /tmp/cpdata.tmpdata
	ENTRY=`head -2 /tmp/cpdata.tmpdata | tail -1 | awk -F\> '{print $2}' | awk -F\< '{print $1}'`
	PKG=`head -4 /tmp/cpdata.tmpdata | tail -1 | awk -F\> '{print $2}' | awk -F\< '{print $1}'`
	PNOC=`head -7 /tmp/cpdata.tmpdata | tail -1 | awk -F\> '{print $2}' | awk -F\< '{print $1}'`
	ACTIVE=`head -9 /tmp/cpdata.tmpdata | tail -1 | sed 's/<br\/>//g' | awk -F\> '{print $2}' | awk -F\< '{print $1}'`
	PROD=`head -10 /tmp/cpdata.tmpdata | tail -1 | awk -F\> '{print $2}' | awk -F\< '{print $1}'`
	STATUS=`head -14 /tmp/cpdata.tmpdata | tail -2 | tr -d '\n' | sed 's/  //g' | sed 's/<br\/>/ /g'`
	UPEX=`head -17 /tmp/cpdata.tmpdata | grep '<td ' | tail -1 | awk -F\> '{print $2}' | awk -F\< '{print $1}'`
	OUTPUT
done

echo "</c>"

## Cleanup
/bin/rm /tmp/cpdata.curltmp /tmp/cpdata.tmpdata
