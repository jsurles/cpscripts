#!/bin/sh
##################################################################
## Variables
##################################################################
FROMMAIL=test@thisdomain.com
TOMAIL=j.surles@cpanel.com
##################################################################
## End Variables
##################################################################

touch /var/log/phpmail.log
chmod 666 /var/log/phpmail.log

echo "mail.add_x_header = On" >> /usr/local/lib/php.ini
echo "mail.log = /var/log/phpmail.log" >> /usr/local/lib/php.ini

echo "<?php" >myphptestmailer.php
echo "ini_set( 'display_errors', 1 );" >>myphptestmailer.php
echo "error_reporting( E_ALL );" >>myphptestmailer.php
echo '$from = "'$FROMMAIL'";' >>myphptestmailer.php
echo '$to = "'$TOMAIL'";' >>myphptestmailer.php
echo '$subject = "PHP Mail Test script";' >>myphptestmailer.php
echo '$message = "This is a test to check the PHP Mail functionality";' >>myphptestmailer.php
echo '$headers = "From:" . $from;' >>myphptestmailer.php
echo 'mail($to,$subject,$message, $headers);' >>myphptestmailer.php
echo 'echo "Test email sent\n";' >>myphptestmailer.php
echo "?>" >>myphptestmailer.php

ls -ld /var/log/phpmail.log
php myphptestmailer.php
ls -ld /var/log/phpmail.log
tail /var/log/phpmail.log
